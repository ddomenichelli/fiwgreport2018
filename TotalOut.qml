import QtQuick 2.0
import QtCharts 2.0

ChartView {
    title: "Expenses"
    legend.alignment: Qt.AlignBottom
    antialiasing: true
    width: 500
    height: 500

    StackedBarSeries {
        id: mySeries
        axisY: ValueAxis {
            min: 0
            max: 375000
            labelFormat: "%d (euros)"
        }
        axisX: BarCategoryAxis { categories: ["2013", "2014", "2015", "2016", "2017", "2018"] }
        BarSet { label: "Expenses (minus Akademy)"; values: [0,0,0,85992,76172, 141569] }
        BarSet { label: "Akademy/QtCon"; values: [0,0,0,253115,71275, 68459] }
        BarSet { label: "Expenses"; values: [160296,122816,79800,0,0,0]; color: "#129eaa"}
    }
}
