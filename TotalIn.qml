import QtQuick 2.0
import QtCharts 2.0

ChartView {
    title: "Income"
    legend.alignment: Qt.AlignBottom
    antialiasing: true
    width: 500
    height: 500

    StackedBarSeries {
        id: mySeries
        axisX: BarCategoryAxis { categories: ["2013", "2014", "2015", "2016", "2017", "2018"] }
        axisY: ValueAxis {
            min: 0
            max: 600000
            labelFormat: "%d (euros)"
        }
        BarSet { label: "Income (minus Akademy)"; values: [111505,122697,78870,136885,121998, 554772] }
        BarSet { label: "Akademy/QtCon"; values: [78787,24864,46940.39,214390,28222, 36844] }

    }
}
