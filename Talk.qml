/***************************************************************************
 *   Copyright (C) 2017 by Eike Hein <hein@kde.org>                        *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

import "presentation"
import QtQuick 2.6

Presentation
{
    id: presentation

    width: square ? 1024 : 1280
    height: square ? 768 : 720

    property bool square: true

    property variant fromSlide: null
    property variant toSlide: null

    property int transitionTime: 400

    fontFamily: "Noto Sans"

    showNotes: false

    Image {
        id: transitionLogo

        visible: false

        width: Math.min(parent.width/2, parent.height/2)
        height: width

        opacity: 0.0

        z: 9999

        anchors.centerIn: parent

        smooth: true
        source: "kdeev.svg"
        sourceSize.width: width
        sourceSize.height: height
    }

    Image {
        opacity: (presentation.currentSlide > 1) ? 1.0 : 0.0

        Behavior on opacity {
            NumberAnimation { duration: presentation.transitionTime/2; easing.type: Easing.InQuart }
        }

        width: (slideCounter.font.pixelSize * 3.2)
        height: width/2.022

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: slideCounter.font.pixelSize

        smooth: true
        source: "kdeev.svg"
        sourceSize.width: width
        sourceSize.height: height
    }

    SequentialAnimation {
        id: forwardTransition
        alwaysRunToEnd: true

        PropertyAction { target: toSlide; property: "visible"; value: true }
        PropertyAction { target: transitionLogo; property: "visible"; value: ((presentation.currentSlide == 0) ? true : false) }
        ParallelAnimation {
            NumberAnimation { target: fromSlide; property: "opacity"; from: 1; to: 0; duration: presentation.transitionTime; easing.type: Easing.OutQuart }
            NumberAnimation { target: fromSlide; property: "scale"; from: 1; to: 1.1; duration: presentation.transitionTime; easing.type: Easing.InOutQuart }
            NumberAnimation { target: toSlide; property: "opacity"; from: 0; to: 1; duration: presentation.transitionTime; easing.type: Easing.InQuart }
            NumberAnimation { target: toSlide; property: "scale"; from: 0.7; to: 1; duration: presentation.transitionTime; easing.type: Easing.InOutQuart }
            NumberAnimation { target: transitionLogo; property: "opacity"; from: 0.4; to: 0.0; duration: presentation.transitionTime; easing.type: Easing.OutQuart }
            NumberAnimation { target: transitionLogo; property: "scale"; from: 0.8; to: 4; duration: presentation.transitionTime; easing.type: Easing.InOutQuart }
        }
        PropertyAction { target: transitionLogo; property: "visible"; value: false }
        PropertyAction { target: fromSlide; property: "visible"; value: false }
        PropertyAction { target: fromSlide; property: "scale"; value: 1 }
    }

    SequentialAnimation {
        id: backwardTransition
        alwaysRunToEnd: true

        PropertyAction { target: toSlide; property: "visible"; value: true }
        ParallelAnimation {
            NumberAnimation { target: fromSlide; property: "opacity"; from: 1; to: 0; duration: presentation.transitionTime; easing.type: Easing.OutQuart }
            NumberAnimation { target: fromSlide; property: "scale"; from: 1; to: 0.7; duration: presentation.transitionTime; easing.type: Easing.InOutQuart }
            NumberAnimation { target: toSlide; property: "opacity"; from: 0; to: 1; duration: presentation.transitionTime; easing.type: Easing.InQuart }
            NumberAnimation { target: toSlide; property: "scale"; from: 1.1; to: 1; duration: presentation.transitionTime; easing.type: Easing.InOutQuart }
        }
        PropertyAction { target: fromSlide; property: "visible"; value: false }
        PropertyAction { target: fromSlide; property: "scale"; value: 1 }
    }

    function switchSlides(from, to, forward)
    {
        if (forwardTransition.running || backwardTransition.running) {
            return false;
        }

        presentation.fromSlide = from;
        presentation.toSlide = to;

        if (forward) {
            forwardTransition.start();
        } else {
            backwardTransition.start();
        }

        return true;
    }

    SlideCounter { id: slideCounter }

    Slide {
        fontFamily: "Noto Sans"

        Image {
            width: parent.height/1.6
            height: width/2.022

            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: -(parent.height/12)

            verticalAlignment: Image.AlignTop

            smooth: true
            source: "kdeev.svg"
            sourceSize.width: width
            sourceSize.height: height
        }

        centeredText: "<h1></h1><h1></h1><h1></h1>\n<h2><b>Financial Working Group</b></h2><h5>— Akademy Report 2019 —</h5>\n<h5>Milan, Italy · September 2019</h5>"
    }

    Slide {
        title: "Members (two-year terms)"

        fontScale: 0.75

        content: [
            "Daniele E. Domenichelli ddomenichelli@drdanz.it (May 19th, 2019)",
            "David Edmundson david@davidedmundson.co.uk (May 19th, 2019)",
            "Roman Gilg subdiff@gmail.com (June 22nd, 2018)",
            "Eike Hein hein@kde.org (Treasurer)",
            "Neofytos Kolokotronis neofytosk@posteo.net (May 19th, 2019)",
            "Marta Rybczynska marta.rybczynska@kde.org (December 12th, 2017)"
        ]
    }

    Slide {
        title: "Mission & Goals"

        fontScale: 0.75

        content: [
            "Support KDE e.V. Board in Financial topics.",
            "In collaboration with the Treasurer, propose financial decisions to the KDE e.V. Board.",
            "Support Board members in commercial activities.",
            "Support the Treasurer in communication actions related to financial topics.",
            "Evaluate the regular financial reports done by the Treasurer.",
            "Help the Community implement the annual Budget Plan.",
        ]
    }

    Slide {
        title: "2019 Budget Plan: Drafting Process"

        content: [
          "The FiWG reviewed the initial draft presented by the Treasurer. Suggested changes included:",
          " Better phrasing",
          " Expanding the Goals part of the budget",
          " Risk analysis"
        ]
    }

    Slide {
        title: "2019 Budget Plan: Mid-year Review"

        content: [
          "The FiWG reviewed the budget status mid-year in detail. The main subjects were:",
          "  Tracking status of Akademy expenses so far",
          "  conf.kde.in and Lakademy status",
          "  Plans to hire a documentation writer in a follow-up project",
          "  Status of the CiviCRM fixes the e.V. has contracted out",
          "  Status of leveraging CiviCRM to stabilize our income",
          "  Currently no plans for fundraising campaigns",
          "  Impact of LAS co-hostin on original sprints budget"
        ]
    }

    Slide {
        title: "KDE e.V. 2018 Annual Report"

        fontScale: 0.85

        content: [
            "The working group helped preparing the financial part of KDE 2018 e.V. Annual Report.",
            "The full year report of the accountant was not available initially, which made the task of extracting the exact numbers manual and error-prone and added to the delay of the Annual Report.",
            "The delay of the yearly financial report is a recurring issue and could be a topic of discussion with our accounting company.",
        ]
    }

    Slide {
        title: "KDE.org Website"

        fontScale: 0.85

        content: [
            "The 2019 Budget Plan has money allocated for a potential project to improve the KDE.org website.",
            "The FiWG will propose a BoF at Akademy to discuss the financial part of the KDE website redesign. The FiWG remarks that with the current amount of reserves, there is a possibility to use a part of this for consultancy and/or outreach."
        ]
    }

    Slide {
        title: "Handshake donation Calligra/Krita"

        fontScale: 0.75

        content: [
            "KDE e.V. received a large donation on behalf of the Calligra team",
            "The Calligra team decided to give 10% of the money to Krita",
            "The KDE e.V.'s lawyers have exhausted all the possible scenarios for giving the money directly to the Krita Foundation.",
            "The FiWG discussed other options and made a recommendation to the board after discussions with Krita and the board.",
            "The recommended solution is to contract a person of the Krita Foundation's choosing through KDE e.V."
        ]
    }

    Slide {
        title: "FiWG Chat"

        content: [
            "The Working Group set up a group chat on KDE's new Matrix instance."
        ]
    }

    Slide {
        centeredText: "Budget Analysis"
    }

    Slide {
        title: "Overview"
        TotalIn{
            anchors.fill: parent
        }
    }
    Slide {
        title: "Overview"
        TotalOut{
            anchors.fill: parent
        }
    }

    Slide {
        centeredText: "Take-aways"
    }

    Slide {
        fontScale: 2
        centeredText: "<b>Q & A</b>"
    }
}
